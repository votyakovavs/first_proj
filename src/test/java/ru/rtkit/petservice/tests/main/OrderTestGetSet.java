package ru.rtkit.petservice.tests.main;

import io.qameta.allure.Description;
import io.restassured.response.Response;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import ru.rtkit.petservice.apiHelper.Endpoints;

import java.util.Arrays;
import java.util.List;
import java.util.Random;


import static org.junit.jupiter.api.Assertions.*;

public class OrderTestGetSet extends BaseTest {
    @ParameterizedTest(name = "{displayName} {0}")
    @DisplayName("Размещение заказа. Имя питомца:")
    @ValueSource(strings = {"Busya", "BUSYA", "busya", "BuSyA", "Буся", "БУСЯ", "буся", "БуСя"})
    @Description("В этом тесте мы создаем питомца и размещаем с ним заказ")
    void newOrder(String petName) {
        PetLombok pet = new PetLombok();
        pet.setPetName(petName);
        pet.setStatus("available");
        Response responseNewPet =
                api.post(Endpoints.NEW_PET, pet, resp200);

        api.get(Endpoints.PET_ID, resp200, responseNewPet.path("id"));

        String[] expectedStatus = {"placed", "approved", "delivered"};
        List<String> expectedTitlesList = Arrays.asList(expectedStatus);
        int n = (int) Math.floor(Math.random() * expectedStatus.length);
        Random random = new Random();
        String petId = responseNewPet.path("id").toString();

        OrderGetSet order = new OrderGetSet();
        order.setPetId(petId);
        order.setQuantity(1);
        order.setShipDate("2022-08-09T15:19:26.234Z");
        order.setStatus(expectedStatus[n]);
        order.setComplete(random.nextBoolean());
        Response responseNewOrder =
                api.post(Endpoints.NEW_ORDER, order, resp200);
        Response responseNewOrderInfo =
                api.get(Endpoints.ORDER_ID, resp200, responseNewOrder.path("id"));

        assertEquals(responseNewOrderInfo.path("petId").toString(), responseNewPet.path("id").toString());
        assertTrue(expectedTitlesList.contains((responseNewOrderInfo.path("status"))));
    }
}
