package ru.rtkit.petservice.tests.main;

import io.qameta.allure.Description;
import io.restassured.response.Response;

import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import ru.rtkit.petservice.apiHelper.Endpoints;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Более продвинутые тесты питомца")
public class PetTestNextTask extends BaseTest {

    @ParameterizedTest(name = "{displayName} {0}")
    @DisplayName("Тест с перебором имен из массива и размещением заказа. Имя питомца:")
    @ValueSource(strings = {"Busya", "BUSYA", "busya", "BuSyA", "Буся", "БУСЯ", "буся", "БуСя"})
    @Description("В этом тесте мы проверяли, что можно добавить питомца с любым именем (вне зависимости от регистра, написанным кириллицей или латиницей), а затем размещали заказ с этим питомцем")
    void checkPetsNamesAndOrders(String petName) {
        JSONObject bodyPet = new JSONObject()
                .put("name", petName)
                .put("status", "available");
        Response responseNewPet =
                api.post(Endpoints.NEW_PET, bodyPet.toString(), resp200);

        api.get(Endpoints.PET_ID, resp200, responseNewPet.path("id"))
                .then()
                .assertThat()
                .body("name", Matchers.matchesPattern(Pattern.compile("^[A-zА-я]+$")));

        String[] expectedStatus = {"placed", "approved", "delivered"};
        List<String> expectedTitlesList = Arrays.asList(expectedStatus);
        int n = (int) Math.floor(Math.random() * expectedStatus.length);
        Random random = new Random();
        JSONObject bodyOrder = new JSONObject()
                .put("petId", responseNewPet.path("id").toString())
                .put("shipDate", "2022-08-09T15:19:26.234Z")
                .put("status", expectedStatus[n])
                .put("complete", random.nextBoolean());
        Response responseNewOrder =
                api.post(Endpoints.NEW_ORDER, bodyOrder.toString(), resp200);
        Response responseNewOrderInfo =
                api.get(Endpoints.ORDER_ID, resp200, responseNewOrder.path("id"));

        assertEquals(responseNewOrderInfo.path("petId").toString(), responseNewPet.path("id").toString());
        assertTrue(expectedTitlesList.contains((responseNewOrderInfo.path("status"))));
        assertInstanceOf(Boolean.class, responseNewOrderInfo.path("complete"));
        assertNotNull(responseNewOrderInfo.path("shipDate"));
    }
}
