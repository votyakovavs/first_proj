package ru.rtkit.petservice.tests.main;

import io.qameta.allure.Description;
import io.qameta.allure.Step;

import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.petservice.apiHelper.Endpoints;

@DisplayName("Простейшие тесты питомца")
public class PetTests extends BaseTest {
    @Test
    @DisplayName("Тест с приключениями Барсика, ставшего Мусей")
    @Description("В этом тесте мы добавляли питомца, меняли его имя, а затем удаляли его.")
    void petAdventures() {

        String petName = "Барсик";
        JSONObject reqBody = new JSONObject()
                .put("id", 25)
                .put("name", petName)
                .put("status", "available");
        long petId = addNewPetToStore(reqBody.toString(), petName);

        JSONObject bodyMusya = new JSONObject()
                .put("id", petId)
                .put("name", "Муся")
                .put("status", "available");
        api.put(Endpoints.NEW_PET, bodyMusya.toString(), resp200);

        api.get(Endpoints.PET_ID, resp200, 25);

        api.delete(Endpoints.PET_ID, resp200, 25);

        api.get(Endpoints.PET_ID, resp404, 25);
    }

    @Step("Добавление питомца {petNames} в магазин ")
    long addNewPetToStore(String pet, String petNames) {
        return api.post(Endpoints.NEW_PET, pet, resp200)
                .jsonPath()
                .getLong("id");
    }
}