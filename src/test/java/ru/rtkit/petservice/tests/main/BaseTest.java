package ru.rtkit.petservice.tests.main;

import io.restassured.specification.ResponseSpecification;
import ru.rtkit.petservice.apiHelper.PetstoreApi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.expect;

public class BaseTest {

    public static ResponseSpecification resp200 = expect().statusCode(200);
    public static ResponseSpecification resp404 = expect().statusCode(404);

   public static Properties props;

    public static PetstoreApi api;

    static {

        String service="service";

        InputStream is = ClassLoader.getSystemResourceAsStream( "service.properties");
        props = new Properties();
        try {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        api = new PetstoreApi();
    }

}
