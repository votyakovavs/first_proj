package ru.rtkit.petservice.tests.main;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetLombok {
    private String petName;
    private String status;
}
