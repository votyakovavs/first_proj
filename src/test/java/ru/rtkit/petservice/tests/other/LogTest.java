package ru.rtkit.petservice.tests.other;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogTest {
    public static Logger LOGGER = LoggerFactory.getLogger(LogTest.class);

    @Test
    public void LogExample() {
        LOGGER.info("Это сообщение будет выведено в лог");
    }

    @Test
    public void LogErr21Game() {
        int a = (int) (Math.random() * 11);
        LOGGER.info("Играем в 21, на руках 16. Вы взяли карту, она " + a + "!");
        if (18 + a == 21) {
            LOGGER.info("Победа!");
        } else {
            LOGGER.error("Не повезло!");
        }
    }
}