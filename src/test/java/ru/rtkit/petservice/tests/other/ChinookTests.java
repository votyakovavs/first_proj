package ru.rtkit.petservice.tests.other;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ChinookTests {
    String pathToDB =
            getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();
    String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Test
    @Step("Получение одного артиста из БД")
    void getArtist() {
        String query = "SELECT * FROM Artists where ArtistId = ?";
        String artist = "";
        try (Connection conn = DriverManager.getConnection(dbUrl)) {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, "1");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                artist = rs.getString("Name");
            } else {
                artist = "Артист c таким id не найден!";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(artist);
    }

    @Test
    @Step("Получение треков в жанре поп из БД")
    void getPopTracks() {
        String query = "SELECT t.* FROM Tracks t LEFT JOIN Genres g ON t.GenreId=g.GenreId where g.Name='Pop'";
        List<String> tracks = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery();
        ) {
            ResultSetMetaData rsmeta = rs.getMetaData();
            while (rs.next()) {
                int cc = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= cc; i++) {
                    if (i == cc) {
                        tracks.add(rsmeta.getColumnName(i) + ": " + rs.getString(i) + "\n");
                    } else {
                        tracks.add(rsmeta.getColumnName(i) + ": " + rs.getString(i));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(tracks);
    }
}
