package ru.rtkit.petservice.apiHelper;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;
import static ru.rtkit.petservice.tests.main.BaseTest.props;

public class PetstoreApi {

    public PetstoreApi() {
        RestAssured.baseURI = props.getProperty("URI");
        RestAssured.defaultParser = Parser.JSON;
    }

    @Step("get {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    @Step("get {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp, Object pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint, pathParams);
    }

    @Step("delete {endpoint}")
    public Response delete(String endpoint, ResponseSpecification resp, Object pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .delete(endpoint, pathParams);
    }

    @Step("post {endpoint}")
    public Response post(String endpoint, Object body, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .log().all()
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

    @Step("put {endpoint}")
    public Response put(String endpoint, Object body, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .put(endpoint);
    }
}
